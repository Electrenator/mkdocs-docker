FROM python:3-slim

ARG LOCATION_MKDOC_BUILD_FILES="./"
ARG NAME_BUILD_SCRIPT_FOLDER
ARG THEME_FOLDER_NAME="theme[s]"
ARG EXTRA_SCRIPTS_FOLDER="extraScript"
ARG REQUIRED_PACKAGES

ENV IMAGE_BUILD_FILES="${LOCATION_MKDOC_BUILD_FILES}" \
	NAME_BUILD_SCRIPT_FOLDER="${NAME_BUILD_SCRIPT_FOLDER}" \
	INTERNAL_MKDOCS_LOCATION="/run/mkdocs/" \
	THEME_FOLDER_NAME="${THEME_FOLDER_NAME}" \
	EXTRA_SCRIPTS_FOLDER="$EXTRA_SCRIPTS_FOLDER"

WORKDIR ${INTERNAL_MKDOCS_LOCATION}

# Install mkdocs' dependancies
RUN apt update && apt install ${REQUIRED_PACKAGES} -y && rm -rf /var/lib/apt/lists/*

# Add needed files
COPY "${IMAGE_BUILD_FILES}/${THEME_FOLDER_NAME}/." "./${THEME_FOLDER_NAME}/"
COPY "${IMAGE_BUILD_FILES}/overrides/." "./${THEME_FOLDER_NAME}/"
COPY "${IMAGE_BUILD_FILES}/${EXTRA_SCRIPTS_FOLDER}" "./${EXTRA_SCRIPTS_FOLDER}"

ADD "${IMAGE_BUILD_FILES}/${NAME_BUILD_SCRIPT_FOLDER}/src/startLocalMkdocs.sh" \
    "${IMAGE_BUILD_FILES}/requirements.txt" ./

# Prepare enviroment for execution (download requirements etc)
RUN bash startLocalMkdocs.sh --prepare

# Add other config files not requiring further configuring / download
ADD "mkdocs.yml" ./

CMD bash startLocalMkdocs.sh
