#!/bin/bash

PIP_ERROR=60
EXTRA_SCRIPTS_ERROR=63
PREPARE_FLAG="--prepare"
VISUAL_SEPERATOR="=-=-=-=-=-=-=-=-=-=-"
DEFAULT_EXTRA_SCRIPTS_FOLDER="extraScript"

set -e # Preserve script errors from sub-processes and pass to this script

if [ ! $EXTRA_SCRIPTS_FOLDER ]; then
	EXTRA_SCRIPTS_FOLDER=$DEFAULT_EXTRA_SCRIPTS_FOLDER;
fi

if [ "$1" == "$PREPARE_FLAG" ]; then
    # Install python requirements
    if [ ! "$INTERNAL_MKDOCS_LOCATION" ]; then
		INTERNAL_MKDOCS_LOCATION=$PWD
        echo "INTERNAL_MKDOCS_LOCATION variable was not yet defind, set to current folder ($INTERNAL_MKDOCS_LOCATION)!"
    fi

    cd "$INTERNAL_MKDOCS_LOCATION"
    pip3 install -r requirements.txt || exit $PIP_ERROR

    # Run extra docker scipts
	if [ -d "$EXTRA_SCRIPTS_FOLDER" ]; then
		for file in "$EXTRA_SCRIPTS_FOLDER/setup/"*.sh; do
			echo "Starting script: $file"
			bash "$file"
		done
	fi;
else
	if [  -d "$EXTRA_SCRIPTS_FOLDER" ]; then
		for file in "$EXTRA_SCRIPTS_FOLDER/startup/"*.sh; do
			echo "Starting script: $file"
			bash "$file"
		done
	fi;

    # Start MKDOCS
    echo "Starting MkDocs now! $VISUAL_SEPERATOR"
    mkdocs serve -a 0.0.0.0:8000
fi;
